<?php

$number = $_POST['number'];

$data['status'] = false;

if (is_numeric($number)) {
    if ($number > 0) {
        $arrayBefore = [];
        for ($i = 0; $i < $number; $i++) {
            array_push($arrayBefore, rand(1, 100));
        }

        $arrayAfter = $arrayBefore;

        asort($arrayAfter);

        $data['status'] = true;
        $data['result'] = implode(', ', $arrayBefore) . '<br>' . implode(', ', $arrayAfter);
    } else {
        $data['result'] = 'Введите число больше нуля!';
    }
} else {
    $data['result'] = 'Введите числовое значение!';
}

echo json_encode($data);